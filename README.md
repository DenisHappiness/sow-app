## Setup Steps

First of all you need to create an App in you Shopify Partner Account. After that upload the project to the public server and setup the environment:

- open the .env file and setup database connection and the app URL
- copy API Key and API Secret from the App settings page in your Shopify Partners acccunt and put it in the .env file or  /config/shopify-app.php file by changeing values for SHOPIFY_API_SECRET and SHOPIFY_API_KEY variables
- to enable uploading products feed to google storage you need to put **services-account.json** configuration file to */storage/app/configs/* folder
- in the server console run command `$ php artisan migrate`
- you're good to go and install the Shopify app from the App home page
