<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExposeboxAccount extends Model
{
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'company_id';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
