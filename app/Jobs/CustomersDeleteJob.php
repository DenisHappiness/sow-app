<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ExposeboxApiController;
use stdClass;

class CustomersDeleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * ExposeboxAccount::class
     *
     * @var class instance
     */
    public $exposebox_account;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain
     * @param stdClass $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;

        $user = resolve(\App\User::class)->where('name', $this->shopDomain->toNative())->first();
        $this->exposebox_account = $user->exposebox_account;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->data) {
            $this->send_to_exposebox($this->data->id);
        }
    }

    /**
     * Deleting the customer from ExposeBox Customers list and change status to 'removed' for Exposebox Members list.
     *
     * @return void
     */
    public function send_to_exposebox($customer_id)
    {
        $exposebox = new ExposeboxApiController($this->exposebox_account['token']);

        // delete customer
        $deleting_customer = $exposebox->delete('customers/'. $customer_id);

        $get_member = $exposebox->get('user-lists/' . $this->exposebox_account['mailing_list_id'] . '/members/' . $customer_id);

        // change mailing list memeber status to 'removed'
        if ($deleting_customer->successful() && $get_member->successful())
        {
            $exposebox->put('user-lists/' . $this->exposebox_account['mailing_list_id'] . '/members/' . $customer_id, array('status' => 'removed'));
        }
    }

}
