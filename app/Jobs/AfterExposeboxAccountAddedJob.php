<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Auth;

class AfterExposeboxAccountAddedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shop = Auth::user();
        $shop_api = $shop->api();
        $themes_request = $shop_api->rest('GET', '/admin/themes.json', [
            'fields' => 'id,role'
        ]);

        if ($themes_request['body']) {
            $themes = $themes_request['body']['container']['themes'];

            if ($themes) {
                foreach ($themes as $theme) {
                    if ($theme['role'] == 'main') {

                        // updating general exposebox pixel snippet with a companyId
                        $pixel = $shop->api()->rest('GET', '/admin/api/2020-07/themes/' . $theme['id'] . '/assets.json', [
                            "asset" => [
                                "key" => "snippets/exposebox-pixel-general.liquid",
                            ]
                        ]);

                        if ($pixel['body'] !== null) {
                            $company_id = $shop->exposebox_account['company_id'];
                            $pixel_value = str_replace('{companyId}', $company_id, $pixel['body']['container']['asset']['value']);
                            $pixel_value = preg_replace('/(var companyId\s*=)\s*([\d]*);/m', "$1 $company_id;", $pixel_value);


                            $shop->api()->rest('PUT', '/admin/api/2020-07/themes/' . $theme['id'] . '/assets.json', [
                                "asset" => [
                                    "key" => "snippets/exposebox-pixel-general.liquid",
                                    "value" => $pixel_value
                                ]
                            ]);
                        }
                    }
                }
            }
        }
    }
}
