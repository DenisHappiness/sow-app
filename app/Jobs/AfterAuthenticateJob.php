<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Auth;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // retrieve themes and find the live one
        $shop = Auth::user();
        $shop_api = $shop->api();

        $themes_request = $shop_api->rest('GET', '/admin/themes.json', [
            'fields' => 'id,role'
        ]);

        if ($themes_request['body']) {
            $themes = $themes_request['body']['container']['themes'];

            if ($themes) {
                foreach ( $themes as $theme ) {
                    if ($theme['role'] == 'main') {

                        // looking for all exposebox pixel snippets and uploading them to the active theme
                        $snippets = Storage::files('/public/shopify/snippets/');

                        $include_code = $first_line_of_include = "  {% include 'exposebox-pixel-general' %}\n";

                        $company_id = $shop->exposebox_account['company_id'];
                        foreach ( $snippets as $snippet_path ) {

                            if(strpos($snippet_path, 'pixel-general')){
                                $pixel_value = str_replace('{companyId}', $company_id, Storage::get($snippet_path));
                                $pixel_value = preg_replace('/(var companyId\s*=)\s*([\d]*);/m', "$1 $company_id;", $pixel_value);
                            } else {
                                $pixel_value = Storage::get($snippet_path);
                            }
                            $shop_api->rest('PUT', '/admin/api/2020-07/themes/' . $theme['id'] . '/assets.json', [
                                "asset" => [
                                    "key" => "snippets/" . basename($snippet_path),
                                    "value" => $pixel_value
                                ]
                            ]);

                            if (basename($snippet_path) !== 'exposebox-pixel-general.liquid' && basename($snippet_path) !== 'exposebox-pixel-checkout.liquid') {
                                $include_code .= "  {% include '" . str_replace('.liquid', '', basename($snippet_path)) . "' %}\n";
                            }
                        }

                        $include_code .= "</head>";


                        // updating theme layout to include all snippets to head
                        $theme_liquid = $shop_api->rest('GET', '/admin/api/2020-07/themes/' . $theme['id'] . '/assets.json', [
                            "asset" => [
                                "key" => "layout/theme.liquid",
                            ]
                        ]);

                        if ($theme_liquid['body'] !== null && strpos($theme_liquid['body']['container']['asset']['value'], $first_line_of_include) === false) {
                            $theme_liquid_value = str_replace('</head>', $include_code, $theme_liquid['body']['container']['asset']['value']);

                            $shop_api->rest('PUT', '/admin/api/2020-07/themes/' . $theme['id'] . '/assets.json', [
                                "asset" => [
                                    "key" => "layout/theme.liquid",
                                    "value" => $theme_liquid_value
                                ]
                            ]);
                        }


                        // updating checkout layout to include customer snippet to head
                        $checkout_liquid = $shop_api->rest('GET', '/admin/api/2020-07/themes/' . $theme['id'] . '/assets.json', [
                            "asset" => [
                                "key" => "layout/checkout.liquid",
                            ]
                        ]);

                        if ($checkout_liquid['body'] !== null) {
                            if (strpos($checkout_liquid['body']['container']['asset']['value'], $first_line_of_include) === false) {
                                $checkout_snippets = "  {% include 'exposebox-pixel-general' %}\n";
                                $checkout_snippets += "  {% include 'exposebox-pixel-customer' %}\n";
                                $checkout_snippets += "  {% include 'exposebox-pixel-checkout' %}\n";
                                $checkout_snippets += '</head>';

                                $checkout_liquid_value = str_replace('</head>', $checkout_snippets, $checkout_liquid['body']['container']['asset']['value']);

                                $shop_api->rest('PUT', '/admin/api/2020-07/themes/' . $theme['id'] . '/assets.json', [
                                    "asset" => [
                                        "key" => "layout/theme.liquid",
                                        "value" => $checkout_liquid_value
                                    ]
                                ]);
                            }
                        } else {
                            $shop_api->rest('DELETE', '/admin/api/2020-07/themes/' . $theme['id'] . '/assets.json', [
                                "asset" => [
                                    "key" => "snippets/exposebox-pixel-checkout.liquid"
                                ]
                            ]);
                        }

                        break;
                    }
                }
            }
        }
    }
}
