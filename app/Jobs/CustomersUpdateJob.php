<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use App\Http\Controllers\ExposeboxApiController;
use stdClass;

class CustomersUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * ExposeboxAccount::class
     *
     * @var class instance
     */
    public $exposebox_account;

    /**
     * Exposebox API Controller
     *
     * @var class instance
     */
    public $exposebox_api;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain
     * @param stdClass $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;

        $user = resolve(\App\User::class)->where('name', $this->shopDomain->toNative())->first();
        $this->exposebox_account = $user->exposebox_account;

        $this->exposebox_api = new ExposeboxApiController($this->exposebox_account['token']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $final_customer = [];

        if ($this->data)
        {
            $customer = (array) $this->data;

            $customer_copy = $customer;
            unset($customer_copy['default_address']);
            unset($customer_copy['accepts_marketing']);
            unset($customer_copy['id']);
            unset($customer_copy['first_name']);
            unset($customer_copy['last_name']);
            $customer_copy['firstName'] = $customer['first_name'];
            $customer_copy['lastName'] = $customer['last_name'];
            $customer_copy['customerId'] = (string) $customer['id'];
            if (isset($customer['default_address'])) {
                $customer['default_address'] = (array) $customer['default_address'];
                $customer_copy['phone'] = $customer['default_address']['phone'];
                $customer_copy['address1'] = $customer['default_address']['address1'];
                $customer_copy['address2'] = $customer['default_address']['address2'];
                $customer_copy['city'] = $customer['default_address']['city'];
                $customer_copy['state'] = $customer['default_address']['province'];
                $customer_copy['country'] = $customer['default_address']['country'];
                $customer_copy['zipCode'] = $customer['default_address']['zip'];
            }
            if (empty($customer_copy['phone'])) {
                $customer_copy['phone'] = $customer['phone'];
            }
            if (isset($customer['accepts_marketing'])) {
                $customer_copy['freeData'] = (object) array('acceptsMarketing' => $customer['accepts_marketing']);
            }
            $final_customer = $customer_copy;

        }

        if ($final_customer)
        {
            $this->send_to_exposebox($final_customer);
        }

    }

    /**
     * Updating customer's info and subsciption settings in ExposeBox Account
     *
     * @return void
     */
    public function send_to_exposebox($customer)
    {
        $default_args = [
            "customerId" => "",
            "email" => "",
            "firstName" => "",
            "lastName" => "",
            "address1" => "",
            "address2" => "",
            "city" => "",
            "country" => "",
            "state" => "",
            "phone" => "",
            "zipCode" => "",
            "freeData" => (object)[]
        ];

        $existing_customer = array_intersect_key( $customer, $default_args );

        // update customer
        $response = $this->exposebox_api->patch('customers/' . $existing_customer['customerId'], $existing_customer);

        if ($response->successful())
        {
            $this->update_eb_mailing_list($existing_customer);
        }
    }

    /**
     * Updating customer's subsciption settings in ExposeBox Account (if not exists - create the list itself and add the memebr)
     *
     * @return void
     */
    public function update_eb_mailing_list($customer)
    {
        $mailing_list_id = $this->exposebox_account['mailing_list_id'];

        if (!empty($mailing_list_id) && $this->eb_mailing_list_exists($mailing_list_id))
        {
            $this->eb_member_update($mailing_list_id, $customer);
        } else {
            $response = $this->exposebox_api->post('user-lists', array('name' => env('EXPOSEBOX_USERS_LIST_NAME', $this->shopDomain->toNative())));

            if ($response->successful())
            {
                $mailing_list_id = $response->body();
                $exposebox_account->update(['mailing_list_id' => $mailing_list_id]);
                $this->eb_member_update($mailing_list_id, $customer);
            }

        }
    }

    /**
     * Update or create member in a certain list
     *
     * @return void
     */
    public function eb_member_update($mailing_list_id, $customer)
    {
        $status = (isset($customer['freeData']->acceptsMarketing) && $customer['freeData']->acceptsMarketing == true) ? 'active' : 'unsubscribed';
        $method = $this->eb_mailing_list_member_exists($mailing_list_id, $customer['customerId']) ? 'put' : 'post';

        return $this->exposebox_api->$method('user-lists/' . $mailing_list_id . '/members', array('customerId' => $customer['customerId'], 'status' => $status, 'customer' => (object)$customer));
    }

    /**
     * Checking if certain ExposeBox Membrs list exists
     *
     * @return void
     */
    public function eb_mailing_list_exists($mailing_list_id)
    {
        return $this->exposebox_api->get('user-lists/' . $mailing_list_id)->successful();
    }

    /**
     * Checking if certain ExposeBox Membrs exists in a certain list
     *
     * @return void
     */
    public function eb_mailing_list_member_exists($mailing_list_id, $customer_id)
    {
        return $this->exposebox_api->get('user-lists/' . $mailing_list_id . '/members/' . $customer_id)->successful();
    }
}
