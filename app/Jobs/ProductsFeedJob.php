<?php

namespace App\Jobs;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Jobs\Middleware\RateLimited;
use Osiset\ShopifyApp\Contracts\ApiHelper as IApiHelper;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use Osiset\ShopifyApp\Services\ShopSession;
use Osiset\ShopifyApp\Objects\Values\AccessToken;
use Osiset\BasicShopifyAPI\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;

class ProductsFeedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * ExposeboxAccount::class
     *
     * @var class instance
     */
    public $exposebox_account;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param stdClass $data The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;

        $user = resolve(\App\User::class)->where('name', $this->shopDomain->toNative())->first();
        $this->exposebox_account = $user->exposebox_account;
    }

    /**
     * Auth shop.
     *
     * @return void
     */
    function get_shop_api() {
        $shop_password = DB::table('users')->where('name', $this->shopDomain->toNative())->value('password');
        $shopSession = resolve(ShopSession::class);
        $token = $shopSession->guest() ? new AccessToken($shop_password) : $shopSession->getToken();

        // Set the session
        $session = new Session(
            $this->shopDomain->toNative(),
            $token->toNative(),
            $shopSession->getUser()
        );
        try {
            return resolve(IApiHelper::class)->make($session)->getApi();
        } catch (BindingResolutionException $e) {
            return null;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $shop_api = $this->get_shop_api();

        // request settings
        $step_count = 250;
        $final_products = array();

        // products num
        $products_count_request = $shop_api->rest('GET', 'admin/products/count.json');

        if($products_count_request['body']['container']['count'] > 0) {
            // collects num
            $collects_count_request = $shop_api->rest('GET', 'admin/collects/count.json');

            // calculating number of requests needed
            $products_steps = ceil($products_count_request['body']['container']['count'] / $step_count);
            $collects_steps = ceil($collects_count_request['body']['container']['count'] / $step_count);

            // getting all products
            $products = [];
            $products_args = array(
                'limit' => $step_count,
                'fields' => 'id,title,handle,variants,images,image,vendor,body_html,product_type'
            );
            for ($i = 0; $i <= $products_steps; $i++) {
                if ($products) {
                    $products_args = array_merge($products_args, array('since_id' => $products[count($products) - 1]['id']));
                } else {
                    $products_args = array_merge($products_args, array('since_id' => 0));
                }
                $request = $shop_api->rest('GET', 'admin/products.json', $products_args);

                $products = array_merge($products, $request['body']['container']['products']);
            }

            // getting all collects
            $collects = [];
            $collects_args = array(
                'limit' => $step_count,
                'fields' => 'id,product_id,collection_id'
            );
            for ($i = 0; $i <= $products_steps; $i++) {
                if ($collects) {
                    $collects_args = array_merge($collects_args, array('since_id' => $collects[count($collects) - 1]['id']));
                } else {
                    $collects_args = array_merge($collects_args, array('since_id' => 0));
                }
                $request = $shop_api->rest('GET', 'admin/collects.json', $collects_args);

                $collects = array_merge($collects, $request['body']['container']['collects']);
            }

            // create collections array
            $collections = array();
            if ( $collects ) {
                foreach ($collects as $collect) {
                    if (! array_key_exists($collect['collection_id'], $collections)) {
                        $collection_request = $shop_api->rest('GET', '/admin/collections/'.$collect['collection_id'].'.json', [
                            'fields' => 'id,title'
                        ]);
                        $collections[$collect['collection_id']] = $collection_request['body']['container']['collection']['title'];
                    }
                }
            }

            // add collections array to all product items
            $products = array_map(function ($array) {
                $array['collections'] = [];
                return $array;
            }, $products);


            foreach( $products as $product ) {
                foreach ($collects as $collect) {
                    if($collect['product_id'] == $product['id']) {
                        $product['collections'][] = $collections[$collect['collection_id']];
                    }
                }

                $product_copy = $product;
                unset($product_copy['variants']);
                unset($product_copy['images']);
                $product_copy['image'] = $product['image']['src'];
                foreach($product['variants'] as $variant) {
                    if ($variant['title'] != 'Default Title') {
                        $product_copy['title'] = $product['title'] . ' - ' . $variant['title'];
                    }
                    $product_copy['id'] = $product['handle'] . '_' . $variant['id'];
                    $product_copy['price'] = $variant['price'];
                    $product_copy['compared_at_price'] = isset($variant['compared_at_price']) ? $variant['compared_at_price'] : 0;

                    if ($variant['image_id']) {
                        foreach($product['images'] as $image) {
                            if ($image['id'] == $variant['image_id']) {
                                $product_copy['image'] = $image['src'];
                            }
                        }
                    }

                    $final_products[] = $product_copy;
                }
            }

        }

        if ($final_products) {
            $this->export($final_products);
        }
    }

    public function export( $products )
    {
        $shop_api = $this->get_shop_api();

        $shop_data = $shop_api->rest('GET', 'admin/shop.json', [
            'fields' => 'currency'
        ]);

        $columns = array('id', 'title', 'price', 'saleOpen', 'url', 'imageUrl', 'supplier', 'catalog', 'category', 'onSale', 'description', 'brand', 'currency', 'freeData');

            $file = fopen('php://temp', 'w');

            fputcsv($file, $columns);

            foreach($products as $product) {
                fputcsv($file, array($product['id'], $product['title'], $product['price'], true, 'https://' . $this->shopDomain->toNative() . '/products/' . $product['handle'], $product['image'], $product['vendor'], ( $product['product_type'] ? $product['product_type'] : 'default' ), implode(', ', $product['collections'] ), $product['price'] > $product['compared_at_price'], $product['body_html'], '', $shop_data['body']['container']['shop']['currency'], ''));
            }

            rewind($file);
            $content = stream_get_contents($file);

            $disk = Storage::disk('gcs');
            if ($disk) {
                // put CSV to google storage
                $disk->put('products-feed-' . $this->exposebox_account['company_id'] . '.csv', $content);
                $url = $disk->url('products-feed-' . $this->exposebox_account['company_id'] . '.csv');
                $this->exposebox_account->update(['products_feed_url' => $url]);
            } else {

                // put CSV to the Shopify Theme assets folder
                $themes_request = $shop_api->rest('GET', '/admin/themes.json', [
                    'fields' => 'id,role'
                ]);

                if ($themes_request['body']) {
                    $themes = $themes_request['body']['container']['themes'];

                    if ($themes) {
                        foreach ($themes as $theme) {
                            if ($theme['role'] == 'main') {
                                $result = $shop_api->rest('PATCH', '/admin/api/2020-07/themes/' . $theme['id'] . '/assets.json', [
                                    "asset" => [
                                        "key" => "assets/products-feed.csv",
                                        "value" => $content
                                    ]
                                ]);

                                if ($result['status'] == 200) {
                                    $this->exposebox_account->update(['products_feed_url' => $result['body']['container']['asset']['public_url']]);
                                }
                            }
                            break;
                        }
                    }
                }
            }

            fclose($file);
    }
}
