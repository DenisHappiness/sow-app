<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ExposeboxApiController;
use stdClass;

class CustomersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * ExposeboxAccount::class
     *
     * @var class instance
     */
    public $exposebox_account;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain
     * @param stdClass $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;

        $user = resolve(\App\User::class)->where('name', $this->shopDomain->toNative())->first();
        $this->exposebox_account = $user->exposebox_account;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $final_customer = [];
        
        if ($this->data)
        {
            $customer = (array) $this->data;

            $customer_copy = $customer;
            unset($customer_copy['default_address']);
            unset($customer_copy['accepts_marketing']);
            unset($customer_copy['id']);
            unset($customer_copy['first_name']);
            unset($customer_copy['last_name']);
            $customer_copy['firstName'] = $customer['first_name'];
            $customer_copy['lastName'] = $customer['last_name'];
            $customer_copy['customerId'] = (string) $customer['id'];
            if (isset($customer['default_address'])) {
                $customer['default_address'] = (array) $customer['default_address'];
                $customer_copy['phone'] = $customer['default_address']['phone'];
                $customer_copy['address1'] = $customer['default_address']['address1'];
                $customer_copy['address2'] = $customer['default_address']['address2'];
                $customer_copy['city'] = $customer['default_address']['city'];
                $customer_copy['state'] = $customer['default_address']['province'];
                $customer_copy['country'] = $customer['default_address']['country'];
                $customer_copy['zipCode'] = $customer['default_address']['zip'];
            }
            if (empty($customer_copy['phone'])) {
                $customer_copy['phone'] = $customer['phone'];
            }
            if (isset($customer['accepts_marketing'])) {
                $customer_copy['freeData'] = (object) array('acceptsMarketing' => $customer['accepts_marketing']);
            }
            $final_customer = $customer_copy;

        }

        if ($final_customer)
        {
            $this->send_to_exposebox($final_customer);
        }

    }

    public function send_to_exposebox($customer)
    {

        $default_args = [
            "customerId" => "",
            "email" => "",
            "firstName" => "",
            "lastName" => "",
            "address1" => "",
            "address2" => "",
            "city" => "",
            "country" => "",
            "state" => "",
            "phone" => "",
            "zipCode" => "",
            "freeData" => (object)[]
        ];

        $new_customer = array_intersect_key( $customer, $default_args );

        $exposebox = new ExposeboxApiController($this->exposebox_account['token']);
        $exposebox->post('customers', $new_customer);

    }
}
