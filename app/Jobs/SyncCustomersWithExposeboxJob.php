<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\ExposeboxApiController;

class SyncCustomersWithExposeboxJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The auth user API
     *
     * @var object
     */
    protected $shop_api;

    /**
     * ExposeboxAccount::class
     *
     * @var class instance
     */
    public $exposebox_account;

    /**
     * Exposebox API Controller
     *
     * @var class instance
     */
    public $exposebox_api;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = auth()->user();
        $this->shop_api = $this->user->api();
        $this->exposebox_account = $this->user->exposebox_account;
        $this->exposebox_api = new ExposeboxApiController($this->exposebox_account['token']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // request settings
        $step_count = 250;
        $final_customers = array();

        // customers num
        $customers_count_request = $this->shop_api->rest('GET', 'admin/products/count.json');

        if($customers_count_request['body']['container']['count'] > 0)
        {

            // calculating number of requests needed
            $customers_steps = ceil($customers_count_request['body']['container']['count'] / $step_count);

            // getting all customers
            $customers = [];
            $customers_args = array(
                'limit' => $step_count,
                'fields' => 'id,email,accepts_marketing,first_name,last_name,default_address'
            );

            for ($i = 0; $i <= $customers_steps; $i++)
            {
                if ($customers)
                {
                    $customers_args = array_merge($customers_args, array('since_id' => $customers[count($customers) - 1]['id']));
                } else {
                    $customers_args = array_merge($customers_args, array('since_id' => 0));
                }
                $request = $this->shop_api->rest('GET', 'admin/customers.json', $customers_args);

                if ($request['status'] == 200)
                {
                    $customers = array_merge($customers, $request['body']['container']['customers']);
                }
            }

            if ($customers)
            {
                foreach($customers as $customer)
                {
                    $customer_copy = $customer;
                    unset($customer_copy['default_address']);
                    unset($customer_copy['accepts_marketing']);
                    unset($customer_copy['id']);
                    unset($customer_copy['first_name']);
                    unset($customer_copy['last_name']);
                    $customer_copy['firstName'] = $customer['first_name'];
                    $customer_copy['lastName'] = $customer['last_name'];
                    $customer_copy['customerId'] = (string) $customer['id'];
                    if (isset($customer['default_address'])) {
                        $customer_copy['phone'] = $customer['default_address']['phone'];
                        $customer_copy['address1'] = $customer['default_address']['address1'];
                        $customer_copy['address2'] = $customer['default_address']['address2'];
                        $customer_copy['city'] = $customer['default_address']['city'];
                        $customer_copy['state'] = $customer['default_address']['province'];
                        $customer_copy['country'] = $customer['default_address']['country'];
                        $customer_copy['zipCode'] = $customer['default_address']['zip'];
                    }
                    if (empty($customer_copy['phone']) && isset($customer['phone'])) {
                        $customer_copy['phone'] = $customer['phone'];
                    }
                    if (isset($customer['accepts_marketing'])) {
                        $customer_copy['freeData'] = (object) array('acceptsMarketing' => $customer['accepts_marketing']);
                    }
                    $final_customers[] = $customer_copy;
                }
            }

        }

        if ($final_customers)
        {
            $this->send_to_exposebox($final_customers);
            $this->create_or_update_eb_mailing_list($final_customers);
        }

    }

    public function send_to_exposebox($customers)
    {

        $default_args = [
            "customerId" => "",
            "email" => "",
            "firstName" => "",
            "lastName" => "",
            "address1" => "",
            "address2" => "",
            "city" => "",
            "country" => "",
            "state" => "",
            "phone" => "",
            "zipCode" => "",
            "freeData" => (object)[]
        ];

        foreach($customers as $customer)
        {
            $new_customer = array_merge( $default_args, $customer );

            $response = $this->exposebox_api->post('customers', $new_customer);

        }

    }
    
    public function create_or_update_eb_mailing_list($customers) {
        $mailing_list_id = $this->exposebox_account['mailing_list_id'];

        if (!empty($mailing_list_id) && $this->eb_mailing_list_exists($mailing_list_id)) {

            $this->add_customers_to_eb_members_list($mailing_list_id, $customers);

        } else {

            $response = $this->exposebox_api->post('user-lists', array('name' => env('EXPOSEBOX_USERS_LIST_NAME', $this->user->name)));

            if ($response->successful()) {
                $mailing_list_id = $response->body();
                $this->exposebox_account->update(['mailing_list_id' => $mailing_list_id]);
                $this->add_customers_to_eb_members_list($mailing_list_id, $customers);
            }

        }

    }

    public function add_customers_to_eb_members_list($mailing_list_id, $customers) {
        foreach($customers as $customer) {
            if (isset($customer['freeData']->acceptsMarketing) && $customer['freeData']->acceptsMarketing == true) {
                $response = $this->exposebox_api->post('user-lists/' . $mailing_list_id . '/members', array('customerId' => $customer['customerId'], 'status' => 'active', 'customer' => (object)$customer));
            }
        }
    }

    public function eb_mailing_list_exists($mailing_list_id) {
        return $this->exposebox_api->get('user-lists/' . $mailing_list_id)->successful();
    }
}
