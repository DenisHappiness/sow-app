<?php

namespace App\Http\Controllers;

use App\ExposeboxAccount;
use App\User;
use App\Http\Controllers\ExposeboxApiController;
use App\Jobs\AfterExposeboxAccountAddedJob;
use App\Jobs\RetrieveProductsFeedJob;
use App\Jobs\SyncCustomersWithExposeboxJob;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use stdClass;

class ExposeboxAccountController extends Controller
{
    public function update_account() {
        $validator = $this->validateAccount();

        if ($validator->fails()) {

            return back()->withErrors($validator)->withInput();

        } else {

            $user_id = Auth()->id();
            $account = ExposeboxAccount::where(['user_id' => $user_id])->first();

            if ($account) {
                $account->update(request()->all());
            } else {
                $validated_data = request()->all();
                $validated_data['user_id'] = $user_id;
                $account = ExposeboxAccount::create($validated_data);
            }

            AfterExposeboxAccountAddedJob::dispatchNow();
            RetrieveProductsFeedJob::dispatchNow();
            SyncCustomersWithExposeboxJob::dispatchNow();

        }

        return redirect()->route('settings')
            ->with('success','Your ExposeBox Account successfully updated.');
    }

    public function validateAccount() {
        $request = request()->all();
        $validator = Validator::make($request, [
            'company_id' => ['required', 'numeric'],
            'token' => 'required'
        ]);

        $validator->after(function ($validator) {
            $exposebox = new ExposeboxApiController(request()->token);
            if (!$exposebox->is_valid_token()) {
                $validator->errors()->add('token', 'Token is invalid');
            }
        });

        return $validator;
    }


    function update($company_id) {
        $account = ExposeboxAccount::find($company_id);
        $account->products_feed_url = request('products_feed_url');
        $account->save();
    }
}
