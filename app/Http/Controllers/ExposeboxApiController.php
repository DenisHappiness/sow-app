<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\ExposeboxAccount;

class ExposeboxApiController extends Controller
{
    /**
     * The ExposeBox API Token
     *
     * @var string
     */
    protected $token = '';

    /**
     * The ExposeBox API URL
     *
     * @var string
     */
    protected $api_url = '';

    /**
     * Instantiate the ExposeBox API Controller.
     *
     * @return void
     */
    public function __construct($token) {
        $this->token = $token;
        $this->api_url = env('EXPOSEBOX_API_URL', 'https://dashboard-v2.exposebox.com/api/v1.0.1/');
    }

    /**
     * Request to ExposeBox API.
     *
     * @return Illuminate\Http\Response
     */
    public function request($method = 'get', $endpoint, $body = array()) {
        return Http::withHeaders([
            "Authorization" => $this->token
        ])->$method($this->api_url . $endpoint, $body);
    }

    /**
     * GET request to ExposeBox API.
     * The 'request' function wrapper
     *
     * @return Illuminate\Http\Response
     */
    public function get($endpoint, $body = array()) {
        return $this->request(__FUNCTION__, $endpoint);
    }

    /**
     * POST request to ExposeBox API.
     * The 'request' function wrapper
     *
     * @return Illuminate\Http\Response
     */
    public function post($endpoint, $body = array()) {
        return $this->request(__FUNCTION__, $endpoint, $body);
    }

    /**
     * PUT request to ExposeBox API.
     * The 'request' function wrapper
     *
     * @return Illuminate\Http\Response
     */
    public function put($endpoint, $body = array()) {
        return $this->request(__FUNCTION__, $endpoint, $body);
    }

    /**
     * PATCH request to ExposeBox API.
     * The 'request' function wrapper
     *
     * @return Illuminate\Http\Response
     */
    public function patch($endpoint, $body = array()) {
        return $this->request(__FUNCTION__, $endpoint, $body);
    }

    /**
     * DELETE request to ExposeBox API.
     * The 'request' function wrapper
     *
     * @return Illuminate\Http\Response
     */
    public function delete($endpoint, $body = array()) {
        return $this->request(__FUNCTION__, $endpoint, $body);
    }

    /**
     * Checking if token is valid and works fine
     *
     * @return Illuminate\Http\Response
     */
    public function is_valid_token() {
        $customer_args = [
            "customerId" => "123",
            "email" => "test@email.com",
        ];

        $this->post('customers/', $customer_args);

        $response = $this->delete('customers/123');

        return $response->successful();
    }
}
