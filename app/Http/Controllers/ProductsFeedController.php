<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Jobs\RetrieveProductsFeedJob;

class ProductsFeedController extends Controller
{
    /**
     * Dispatching products feed generating job
     *
     */
    public function index()
    {
        RetrieveProductsFeedJob::dispatchNow();
    }

    public function products_feed($companyId) {
        $feed_url = DB::table('exposebox_accounts')->where('company_id', $companyId)->value('products_feed_url');

        return redirect($feed_url);
    }

}
