<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\SyncCustomersWithExposeboxJob;

class CustomersFeedController extends Controller
{
    /**
     * Dispatching sync customers with ExposeBox job
     *
     */
    public function index()
    {
        SyncCustomersWithExposeboxJob::dispatchNow();
    }

}
