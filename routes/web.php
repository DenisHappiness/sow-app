<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Main App pages */
// home
Route::get('/', function () {
    if(Auth()->user()->exposebox_account['company_id']) {
        $shop = Auth::user();
        $disk = Storage::disk('gcs');
        $url = $disk ? $shop->exposebox_account['products_feed_url'] : url('/products-feed/'.$shop->exposebox_account['company_id']);
        
        return view('welcome', ['products_feed_url' => $url]);
    } else {
        return redirect(route('settings'));
    }
})->middleware(['auth.shopify'])->name('home');;
// settings
Route::get('/settings', function () {
    return view('settings');
})->middleware(['auth.shopify'])->name('settings');


// Shopify API Controller
Route::get('shopify', 'ShopifyController@index')->middleware(['auth.shopify']);


// Update ExposeBox Account Info
Route::post('/exposebox-connect', 'ExposeboxAccountController@update_account')->middleware(['auth.shopify'])->name('exposebox-connect');


// Products Feed generation endpoint
Route::post('/webhook/products-feed', 'ProductsFeedController@index')->middleware(['auth.shopify','auth.webhook']);
Route::get('/products-feed/{companyId}', 'ProductsFeedController@products_feed');