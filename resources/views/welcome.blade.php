@extends('shopify-app::layouts.default')

@section('styles')
    @parent
    <!-- Main Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endsection

@section('content')
    @php
        $shop = Auth::user();
        $shop_api = $shop->api();
    @endphp
    <main class="Polaris-Frame__Main" id="AppFrameMain" data-has-global-ribbon="false">
        <a id="AppFrameMainContent" tabindex="-1"></a>
        <div class="Polaris-Frame__Content">
            <div class="Polaris-Page">
                <div class="Polaris-Page-Header">
                    <div class="Polaris-Page-Header__MainContent">
                        <div class="Polaris-Page-Header__TitleActionMenuWrapper">
                            <div>
                                <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                                    <div class="Polaris-Header-Title">
                                        <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">ExposeBox Integration</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="Polaris-Page__Content">
                    <div class="Polaris-Layout">
                        <a id="SkipToContentTarget" tabindex="-1"></a>

                        <div class="Polaris-Layout__AnnotatedSection">
                            <div class="Polaris-Layout__AnnotationWrapper">
                                <div class="Polaris-Layout__Annotation">
                                    <div class="Polaris-TextContainer">
                                        <h2 class="Polaris-Heading">Integration status</h2>
                                        <div class="Polaris-Layout__AnnotationDescription">
                                            <p>Checking the published theme for a proper ExposeBox integration.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="Polaris-Layout__AnnotationContent">
                                    <div class="Polaris-Card">
                                        <div class="Polaris-DataTable">
                                            <div class="Polaris-DataTable__ScrollContainer">
                                                <table class="Polaris-DataTable__Table">
                                                    <thead>
                                                        <tr>
                                                            <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header" scope="col">Snippet Name</th>
                                                            <th data-polaris-header-cell="true" class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header" scope="col">Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $themes_request = $shop_api->rest('GET', '/admin/themes.json', [
                                                                'fields' => 'id,role'
                                                            ]);

                                                            if ($themes_request['body']) {
                                                                $themes = $themes_request['body']['container']['themes'];

                                                                if ($themes) {
                                                                    foreach ( $themes as $theme ) {
                                                                        if ($theme['role'] == 'main') {
                                                                            $theme_id = $theme['id'];
                                                                            $theme_liquid = $shop_api->rest('GET', '/admin/api/2020-07/themes/' . $theme_id . '/assets.json', [
                                                                                "asset" => [
                                                                                    "key" => "layout/theme.liquid",
                                                                                ]
                                                                            ]);
                                                                            $theme_liquid_code = $theme_liquid['body']['container']['asset']['value'];

                                                                            $checkout_liquid = $shop_api->rest('GET', '/admin/api/2020-07/themes/' . $theme_id . '/assets.json', [
                                                                                "asset" => [
                                                                                    "key" => "layout/checkout.liquid",
                                                                                ]
                                                                            ]);
                                                                            $checkout_liquid_code = $checkout_liquid['body']['container']['asset']['value'];

                                                                            if ($theme_liquid_code || $checkout_liquid_code) {
                                                                                $snippets = Storage::files('/public/shopify/snippets/');
                                                                                foreach ( $snippets as $snippet_path ) {
                                                                                    $snippet_name = str_replace('.liquid', '', basename($snippet_path));

                                                                                    echo '<tr class="Polaris-DataTable__TableRow">
                                                                                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignCenter Polaris-DataTable__Cell--firstColumn" scope="row">' . str_replace('.liquid', '', basename($snippet_path)) . '</th>';
                                                                                    if ((strpos($snippet_name, 'checkout') == false && strpos($theme_liquid_code, "{% include '".$snippet_name."' %}") !== false) || (strpos($snippet_name, 'checkout') !== false && $checkout_liquid_code && strpos($checkout_liquid_code, "{% include '".$snippet_name."' %}") !== false)) {
                                                                                        echo '<td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop">
                                                                                          <div class="Polaris-Banner Polaris-Banner--statusInfo Polaris-Banner--withinContentContainer" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner22Content">
                                                                                            <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorTealDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                                                                  <circle cx="10" cy="10" r="9" fill="currentColor"></circle>
                                                                                                  <path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8m1-5v-3a1 1 0 0 0-1-1H9a1 1 0 1 0 0 2v3a1 1 0 0 0 1 1h1a1 1 0 1 0 0-2m-1-5.9a1.1 1.1 0 1 0 0-2.2 1.1 1.1 0 0 0 0 2.2"></path>
                                                                                                </svg></span></div>
                                                                                            <div class="Polaris-Banner__ContentWrapper">
                                                                                              <div class="Polaris-Banner__Content" id="Banner22Content">
                                                                                                <p>Installed successfully!</p>
                                                                                              </div>
                                                                                            </div>
                                                                                          </div>
                                                                                        </td>';
                                                                                    } else {
                                                                                         echo '<td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop">
                                                                                         <div class="Polaris-Banner Polaris-Banner--statusWarning Polaris-Banner--withinContentContainer" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner22Content">
                                                                                            <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorYellowDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8m0-13a1 1 0 0 0-1 1v4a1 1 0 1 0 2 0V6a1 1 0 0 0-1-1m0 8a1 1 0 1 0 0 2 1 1 0 0 0 0-2"></path></svg></span></div>
                                                                                            <div class="Polaris-Banner__ContentWrapper">
                                                                                              <div class="Polaris-Banner__Content" id="Banner22Content">
                                                                                                <p>Not installed!</p>
                                                                                              </div>
                                                                                            </div>
                                                                                          </div>
                                                                                         </td>';
                                                                                    }
                                                                                    echo '</tr>';
                                                                                }
                                                                            }

                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        @endphp
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($shop->exposebox_account['company_id'] && $shop->exposebox_account['products_feed_url'])
                            <div class="Polaris-Layout__AnnotatedSection">
                                <div class="Polaris-Layout__AnnotationWrapper">
                                    <div class="Polaris-Layout__Annotation">
                                        <div class="Polaris-TextContainer">
                                            <h2 class="Polaris-Heading">Products feed</h2>
                                            <div class="Polaris-Layout__AnnotationDescription">
                                                <p>This is the URL to CSV file which you can use for ExposeBox account settings.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Polaris-Layout__AnnotationContent">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-Labelled__LabelWrapper">
                                                    <div class="Polaris-Label">
                                                        <label for="company_id" class="Polaris-Label__Text">Feed URL</label>
                                                    </div>
                                                </div>
                                                <div class="Polaris-Connected">
                                                    <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                        <div class="Polaris-TextField">
                                                            <input class="Polaris-TextField__Input" type="text" value="{{ $products_feed_url }}" readonly>
                                                            <div class="Polaris-TextField__Backdrop"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="Polaris-Layout__AnnotatedSection">
                            <div class="Polaris-Layout__AnnotationWrapper">
                                <div class="Polaris-Layout__Annotation">
                                    <div class="Polaris-TextContainer">
                                        <h2 class="Polaris-Heading">Tracking Thankyou page</h2>
                                        <div class="Polaris-Layout__AnnotationDescription">
                                            <p>There's no way to automatically integrate this script so you have to manually copy and paste it to a corresponding field.</p>
                                            <br />
                                            <p>To track orders you need to paste the following code to the "Additional scripts" field of the <a href="https://{{ $shop->name }}/admin/settings/checkout#PolarisTextField1" target="_blank" class="Polaris-Link">Checkout Settings</a> section.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="Polaris-Layout__AnnotationContent">
                                    <pre class="Polaris-TextStyle--variationCode" id="code" onclick="selectText(this.id);" style="background: #fff; padding: 15px; border-radius: 3px;">{{ str_replace('{companyId}', $shop->exposebox_account['company_id'], Storage::get('/public/shopify/manual/exposebox-pixel-thankyou.liquid')) }}</pre>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
        var AppBridge = window['app-bridge'];
        var actions = AppBridge.actions;
        var TitleBar = actions.TitleBar;
        var Button = actions.Button;
        var Redirect = actions.Redirect;
        var titleBarOptions = {
            title: 'Welcome',
        };
        var myTitleBar = TitleBar.create(app, titleBarOptions);
    </script>

    <script type="text/javascript">
        function selectText(id){
            var sel, range;
            var el = document.getElementById(id); //get element id
            if (window.getSelection && document.createRange) { //Browser compatibility
                sel = window.getSelection();
                if(sel.toString() == ''){ //no text selection
                    window.setTimeout(function(){
                        range = document.createRange(); //range object
                        range.selectNodeContents(el); //sets Range
                        sel.removeAllRanges(); //remove all ranges from selection
                        sel.addRange(range);//add Range to a Selection.
                    },1);
                }
            }else if (document.selection) { //older ie
                sel = document.selection.createRange();
                if(sel.text == ''){ //no text selection
                    range = document.body.createTextRange();//Creates TextRange object
                    range.moveToElementText(el);//sets Range
                    range.select(); //make selection.
                }
            }
        }
    </script>
@endsection
