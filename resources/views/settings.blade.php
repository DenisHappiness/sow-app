@extends('shopify-app::layouts.default')

@section('styles')
    @parent
    <!-- Main Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        var AppBridge = window['app-bridge'];
        var actions = AppBridge.actions;
        var TitleBar = actions.TitleBar;
        var Button = actions.Button;
        var Redirect = actions.Redirect;
        var titleBarOptions = {
            title: 'Settings',
        };
        var myTitleBar = TitleBar.create(app, titleBarOptions);
    </script>
@endsection

@section('content')
<main class="Polaris-Frame__Main" id="AppFrameMain" data-has-global-ribbon="false">
    <a id="AppFrameMainContent" tabindex="-1"></a>
    <div class="Polaris-Frame__Content">
        <div class="Polaris-Page">
            <div class="Polaris-Page-Header">
                <div class="Polaris-Page-Header__MainContent">
                    <div class="Polaris-Page-Header__TitleActionMenuWrapper">
                        <div>
                            <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                                <div class="Polaris-Header-Title">
                                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">ExposeBox Integration</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="Polaris-Page__Content">
                <div class="Polaris-Layout">
                    <a id="SkipToContentTarget" tabindex="-1"></a>
                    <div class="Polaris-Layout__AnnotatedSection">
                        <div class="Polaris-Layout__AnnotationWrapper">
                            <div class="Polaris-Layout__Annotation">
                                <div class="Polaris-TextContainer">
                                    <h2 class="Polaris-Heading">ExposeBox Account settings</h2>
                                    <div class="Polaris-Layout__AnnotationDescription">
                                        <p>CompanyId will be used to integrate ExposeBox Pixel scripts. Token is needed to sync customers and products with ExposeBox platform.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="Polaris-Layout__AnnotationContent">
                                @if (session('success'))
                                    <div class="Polaris-Page__Content">
                                        <div class="Polaris-Banner Polaris-Banner--statusSuccess Polaris-Banner--hasDismiss Polaris-Banner--withinPage" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner10Heading" aria-describedby="Banner10Content">
                                            <div class="Polaris-Banner__Dismiss">
                                                <button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Dismiss notification">
                                                    <span class="Polaris-Button__Content">
                                                        <span class="Polaris-Button__Icon">
                                                            <span class="Polaris-Icon">
                                                                <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                                    <path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                                                                </svg>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </button>
                                            </div>
                                            <div class="Polaris-Banner__Ribbon">
                                                <span class="Polaris-Icon Polaris-Icon--colorGreenDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop">
                                                    <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                      <circle fill="currentColor" cx="10" cy="10" r="9"></circle>
                                                      <path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8m2.293-10.707L9 10.586 7.707 9.293a1 1 0 1 0-1.414 1.414l2 2a.997.997 0 0 0 1.414 0l4-4a1 1 0 1 0-1.414-1.414"></path>
                                                    </svg>
                                                </span>
                                            </div>
                                            <div class="Polaris-Banner__ContentWrapper">
                                                <div class="Polaris-Banner__Heading" id="Banner10Heading">
                                                    <p class="Polaris-Heading">{{ session('success') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        header( "refresh:3;url=". route('home') );
                                    @endphp
                                @endif

                                <form method="POST" action="{{ route('exposebox-connect') }}">
                                    @csrf
                                    <div class="Polaris-Card">
                                        <div class="Polaris-Card__Section">
                                            <div class="Polaris-FormLayout">
                                                <div class="Polaris-FormLayout__Item">
                                                    <div class="Polaris-Labelled__LabelWrapper">
                                                        <div class="Polaris-Label">
                                                            <label for="company_id" class="Polaris-Label__Text">CompanyID</label>
                                                        </div>
                                                    </div>
                                                    <div class="Polaris-Connected">
                                                        <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                            <div class="Polaris-TextField">
                                                                <input name="company_id" id="company_id" class="Polaris-TextField__Input" type="text"{{ $errors->has('company_id') ? ' class="error"' : '' }} value="{{ old('company_id') ?? Auth()->user()->exposebox_account['company_id'] }}" required>
                                                                <div class="Polaris-TextField__Backdrop"></div>
                                                            </div>
                                                            @if ($errors->has('company_id'))
                                                                <div class="Polaris-InlineError">
                                                                    <p class="error-message">{{ $errors->first('company_id') }}</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="Polaris-FormLayout__Item">
                                                    <div class="Polaris-Labelled__LabelWrapper">
                                                        <div class="Polaris-Label">
                                                            <label for="token" class="Polaris-Label__Text">Token</label>
                                                        </div>
                                                    </div>
                                                    <div class="Polaris-Connected">
                                                        <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                            <div class="Polaris-TextField">
                                                                <input name="token" id="token" class="Polaris-TextField__Input" type="text"{{ $errors->has('token') ? ' class="error"' : '' }} value="{{ old('token') ?? Auth()->user()->exposebox_account['token'] }}" required>
                                                                <div class="Polaris-TextField__Backdrop"></div>
                                                            </div>
                                                            @if ($errors->has('token'))
                                                                <div class="Polaris-InlineError">
                                                                    <p class="error-message">{{ $errors->first('token') }}</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="Polaris-PageActions">
                                        <div class="Polaris-Stack Polaris-Stack--spacingTight Polaris-Stack--distributionTrailing">
                                            <div class="Polaris-Stack__Item">
                                                <input type="submit" class="Polaris-Button Polaris-Button--primary" value="Save">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection